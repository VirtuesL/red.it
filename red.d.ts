export interface reddit{
  type: string, 
  time: string, 
  subreddit: string, 
  posts:Array<{title: string, permalink: string, author: string, score: number, over_18: boolean, date: Date, url: string}>, 
  after: string, 
  before:string
}