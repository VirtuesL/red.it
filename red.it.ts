import { default as axios, AxiosInstance, AxiosResponse, AxiosError } from 'axios';
import { reddit } from './red'

const AxCli: AxiosInstance = axios.create({
  baseURL: `https://reddit.com/`
});

export const fetchSubreddit = async (sub:string, type:string ='', time:string ='',after:string ='') => {
  return AxCli.get(`${sub}/${type}.json${time? `?t=${time}`:""}${after? `?after=${after}`:""}`)
    .then((res: AxiosResponse) => {
      return <reddit> {
        type: type,
        time: time,
        subreddit: res.data.data.children[0].data.subreddit_name_prefixed,
        posts: res.data.data.children.map((post: any)=>{
          return {
            title: post.data.title,
            permalink: post.data.permalink,
            author: post.data.author,
            score: post.data.score,
            over_18: post.data.over_18,
            date: new Date(post.data.created_utc * 1000),
            url: post.data.url,
            generateLink: async function(){return res.config.baseURL? res.config.baseURL.slice(0,-1) + this.permalink: undefined}
        }
      }),
      after: res.data.data.after,
      before: res.data.data.before,
      next: async function(){
        return fetchSubreddit(this.subreddit, this.type, this.time, this.after)
          .catch((error: AxiosError)=>{throw new Error(error.message)})
        }
    }})
    .catch((error:AxiosError)=>{throw new Error(error.message)})
};